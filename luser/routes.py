from flask import render_template, request, redirect
from luser import app
from luser.models import LUSER
from passlib.hash import ldap_salted_sha1,sha512_crypt
import subprocess
import random
import base64
import os
import configparser

CONFIG_PATH = "/var/luser/luser/config.ini"

config = configparser.ConfigParser()
config.read(CONFIG_PATH)

LDAPHOST = config.get('credentials', 'LDAPHOST')
LDAPADMINNAME = config.get('credentials', 'LDAPADMINNAME')
LDAPPASS = config.get('credentials', 'LDAPPASS')
USERBASE = config.get('credentials', 'USERBASE')
ALTUSERBASE = config.get('credentials', 'ALTUSERBASE')
CAPTCHA_PATH = config.get('credentials', 'CAPTCHA_PATH')

@app.route('/account/changepassword/', methods=['POST', 'GET'])
def changepassword():
    if request.method == 'GET':
        return render_template('changepassword.html')
    elif request.method == 'POST':
        username = request.form['username']
        oldpassword = request.form['oldpassword']
        newpassword = request.form['newpassword']

        # Check lenght of password
        if len(newpassword) < 8:
            return 'Error: password is too short'

        # Create a LUSER connection
        luser = LUSER(LDAPHOST,LDAPADMINNAME,LDAPPASS,USERBASE,ALTUSERBASE)

        if ldap_salted_sha1.verify(oldpassword, luser.getpassword(username)) == False:
                return 'Wrong username/password combination'

        ldaphash = ldap_salted_sha1.hash(newpassword)
        althash = sha512_crypt.hash(newpassword)

        # Try to change user password
        try:
            if luser.changepassword(username, ldaphash, althash) == True:
                return 'User password successfuly changed'
            else:
                return 'User password change failed'
        except:
            return 'User password change failed, exception raised'
    else:
        return 'HTTP request method not recogniezed'

@app.route('/account/unregister/', methods=['POST', 'GET'])
def unregister():
    if request.method == 'GET':
        return render_template('unregister.html')
    elif request.method == 'POST':
        username = request.form['username']
        password = request.form['password']

        # Create a LUSER connection
        luser = LUSER(LDAPHOST,LDAPADMINNAME,LDAPPASS,USERBASE,ALTUSERBASE)

        if ldap_salted_sha1.verify(password, luser.getpassword(username)) == False:
                return 'Wrong username/password combination'

        # Try to delete user
        try:
            if luser.delete(username) == True:
                return 'User successfuly unregistered'
            else:
                return 'User unregistration failed'
        except:
            return 'User unregistration failed, exception raised'
    else:
        return 'HTTP request method not recogniezed'

@app.route('/account/register/', methods=['POST', 'GET'])
def register():
    if request.method == 'GET':
        captcha_solution = str(random.randint(0,999999))
        captcha_hash = ldap_salted_sha1.hash(captcha_solution)
        captcha_filename = base64.b64encode(captcha_hash.encode('utf-8')).decode('utf-8')[8:-1] + ".png"
        captcha_path = CAPTCHA_PATH + captcha_filename
        captcha_file = open(captcha_path, 'w')
        subprocess.run(["/usr/local/bin/captcha.sh",captcha_solution],stdout=captcha_file)
        return render_template('register.html',imgsrc=captcha_filename,captchahash=captcha_hash)
    elif request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        confirmpassword = request.form['confirmpassword']
        captcha_answer = request.form['captchaa']
        captcha_filename = request.form['captchaq']
        captcha_path = CAPTCHA_PATH + captcha_filename

        if captcha_filename[:-4].isalnum() == False and len(captcha_filename) != 47 :
            return 'Error: Captcha question is manipulated'

        captcha_valid = True

        if os.path.exists(captcha_path) == False:
            captcha_valid = False
        else:
            os.remove(captcha_path)

        captcha_hash = base64.b64decode(base64.b64encode(b"{SSHA}").decode('utf-8') + captcha_filename[:-4] + "=").decode('utf-8')

        if ldap_salted_sha1.verify(captcha_answer, captcha_hash) == False:
            captcha_valid = False

        if captcha_valid == False:
            return 'Error: Captcha is wrong!'

        # Check lenght of password
        if len(password) < 8:
            return 'Error: password is too short'

        # Check if passwords matches
        if password != confirmpassword:
            return 'Error: passwords do not match'

        # Check if username has uppercase
        if username.islower() == False:
            return 'Error: uppercase characters in username are not allowed'

        # Check lenght of username
        if len(username) < 1 or len(username) > 30:
            return 'Error: username has to be between 1 and 30 characters long'

        # Check if username is alphanumeric
        if not username.isalnum():
            return 'Error: username can only contain letters and numbers'

        # Create a LUSER connection
        luser = LUSER(LDAPHOST,LDAPADMINNAME,LDAPPASS,USERBASE,ALTUSERBASE)
        # Try to add user
        try:
            ldaphash = ldap_salted_sha1.hash(password)
            althash = sha512_crypt.hash(password)
            #smtpctlout=subprocess.run(["smtpctl","encrypt", password],text=True,stdout=subprocess.PIPE)
            #smtpdhash=smtpctlout.stdout[:-1]
            if luser.add(username,ldaphash,althash):
                return 'User successfuly registered'
            else:
                return 'User registration failed, username probably taken'
        except:
            return 'User registration failed, exception raised'
    else:
        return 'HTTP request method not recogniezed'
